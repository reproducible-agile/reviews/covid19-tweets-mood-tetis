[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4742152.svg)](https://doi.org/10.5281/zenodo.4742152)

# Hierarchical_TFIDF applied on covid-19 tweets

This project aims to extract discriminative terms on spatial and time windows. 
Experimentations are lead on covid-19 on the corpus of tweets created by Emily Chen : https://github.com/echen102/COVID-19-TweetIDs

## Prerequisites (Linux based instructions):
### Download this repository
1. Create an empty directory and open it: `mkdir htfidf && cd htfidf`
2. Git clone this repository: `git clone https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis.git`
### Install Python requirements 
1. Make sur you have Python3 (with pip) installed in your computer
2. Install virtualenv: `pip3 install virtualenv`
3. Create a python virtual environnement: `virtualenv htfidfvenv`
4. Use python3 in your virtualenv: `virtualenv -p /usr/bin/python3 htfidfvenv`
5. Activate your virtualenv: `source htfidfvenv/bin/activate`
6. Install this repository requirements: `pip3 install -r covid19-tweets-mood-tetis/requirements.txt`
### Install Elastic stack: Elasticsearch, Logstash and Kibana (ELK)

Installation of ELK and plugins:

1. Install ELK (version used by authors: 7.10): logstash, elasticsearch and kibana:
    + `sudo apt install elasticsearch`
    + `sudo apt install logstash`
    + `sudo apt install kibana`
2. Install a plugin for logstash to geocode user location (plugin is for using API Rest):
    `sudo /usr/share/logstash/bin/logstash-plugin install logstash-filter-rest`
3. Logstash need an absolute path to data. So we can create a Linux environnement var to retrieve absolute path: `export PWD=$(pwd)`
4. Start elasticsearch and Kibana daemon (Logstash will be run in standalone):
    + `sudo systemctl start elasticsearch`
    + `sudo systemctl start kibana`

You can also install [ELK with docker](https://www.elastic.co/guide/en/elastic-stack-get-started/master/get-started-docker.html)
    
## Run H-TFIDF workflow:
If you want to run a mini workflow, see [reproducibility AGILE'2021 section](https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis#run-a-mini-reproducibility-workflow-for-agile2021).

Otherwise, you can follow instructions below to run a complete workflow set up according to your needs
### Retrieve (hydrate) tweets from E.Chen and Geocode it
Tweets have to be download (i.e. hydrated) from E.Chen repository and indexed into an Elasticsearch index. See steps belows :
1. Create a directory which will contains E.Chen tweets: `mkdir tweets && cd tweets` 
2. Git clone [E.Chen](https://github.com/echen102/COVID-19-TweetIDs): `git clone https://github.com/echen102/COVID-19-TweetIDs.git`
    . On 2021-05-05, 2 hours were needed to download E.Chen repository (~8GB)
3. Edit the script "hydrate.py" from E.Chen repository to select your time period. `vim hydrate.py` and [remove non-wanted months on line 20](https://github.com/echen102/COVID-19-TweetIDs/blob/master/hydrate.py#L20). For exemple, if you want to run H-TFIDF only working on January 2020, the data-dir should be: `data_dirs = ['2020-01']`.
3. Configure [twarc](https://twarc-project.readthedocs.io/en/latest/) to download tweets from Twitter’s API (provides your consumer key & secret and your API key & secret): `twarc configure`
3. Launch echen hydrate script: `python3 hydrate.py`
4. Create a repository to unzip tweets: `mkdir tweets/hydrating-and-extracting`
4. Copy all hydrating tweets. There are zipped :
        `find . -name '*.jsonl.gz' -exec cp -prv '{}' 'tweets/hydrating-and-extracting' ';'`
5. Unzip all json.gz : `gunzip hydrating-and-extracting/coronavirus-tweet*`
6. Index in a Elastic Search  :
    1. Start indexing in elastic and geocoding with logstash and photon/OSM:
        * `sudo /usr/share/logstash/bin/logstash -f covid19-tweets-mood-tetis/elasticsearch/logstash-config/json.conf`
        * /!\ Be carefull if you try to index with a laptop using Wifi, it may power off wlan interface even if you desable sleep mode. If you are using a debian/ubuntu OS, you'll need to disable power management on your wlan interface. =>
        `sudo iwconfig wlo1 power off` (non permanent on reboot)
    2. (OPTIONAL) : Kibana : you can import [dashboard](elasticsearch/kibana-dashboard)

### Run the script
The main script, [COVID-19-TweetIDS-ES-Analyse.py](COVID-19-TweetIDS-ES-Analyse.py), allows to :
+ Build a Hiearchical TF-IDF called H-TFIDF over space and time
+ Build classical TF-IDF to compare with
+ Encode both extracted terms from previous measures to compute semantic similarity :

`python3 COVID-19-TweetIDS-ES-Analyse.py`

More experimentations or methods for evaluate H-TFIDF compared with a classical TF-IDF can be found [script](exploration_data_analyse/eda-es.py) and [explaination](readme_ressources/eda_es.md)

### OPTIONAL Scripts:
in order to **explore the dataset without using elastic search** (except from one of them), here are some scripts that allow to have first results :

* [Dataset Analysis](exploration_data_analyse/COVID-19-TweetIDs-dataset-analyse.py) : some stats computed on Echen corpus
* [Extractor](exploration_data_analyse/COVID-19-TweetIDs-extractor.py) : script extracting only tweets'contents (without RT) in order to share data without all twitter's verbose API 
* And a Pipeline for terms extraction using biotex :
    1. [preprocess](exploration_data_analyse/COVID-19-TweetIDs-preprocess.py) : cleaning up tweets and building corpus in the biotex syntaxe
    2. [biotex-wrapper](exploration_data_analyse/COVID-19-TweetsIDS_biotex_wrapper.py): An automatisation of biotex on 4 settings
    3. [merge biotex results](exploration_data_analyse/COVID-19-TweetIDS-merge-biotex-results.py): Due to the size of this corpus, biotex could not be launched on the full corpus. It has to be splitt in 30k tweets. Then results have to be merged and ranked
* [Other fonctions to explore but which use Elasticsearch](exploration_data_analyse/eda-es.py)
    
This is based upon works of:
* **Juan Antonio LOSSIO-VENTURA** creator of [BioTex](https://github.com/sifrproject/biotex/tree/master)
* **Jacques Fize** who build a python wrapper of Biotext (see [his repository](https://gitlab.irstea.fr/jacques.fize/biotex_python) for more details)
* **Gaurav Shrivastava** who code FASTR algorithme in python. His script is in this repository

NB : **Due to the size of this corpus, biotex could not be launched on the full corpus. It has to be splitt in 30k tweets. Then results have to be merged and ranked**

## Run a mini reproducibility workflow for AGILE'2021
**To set up this workflow ~1 hour is needed (10min to hydrate tweets and 45min to geocode it)**

Twitter's API has limitations. Retrieving tweets (also called **hydrating**) is a time-consuming task. A hydrating task calls the Twitter's [status/lookup API](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets), which is limited to 300 requests every 15 min and each request can hydrate 100 tweets, i.e. **120k tweets/hours**.
For this reproducibility workflow, we are going to work on a small subset from E.Chen (10 000 Tweets):
+ Time period: from 2020-01-22 to 2020-01-31
+ Spatial extent: Twitter users that may be in an European country (according to their user.location description)
+ lang: English

### Instructions: 
0. Make sure all [prerequisites](https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis#prerequisites-linux-based-instructions) are done (you activated the python virtualenv, with linux environnement var "PWD", elasicsearch running and this repository already cloned)
1. Configure twarc: `twarc configure`
2. Change directory to be at the root of this repository: `cd covid19-tweets-mood-tetis/`
3. Create a linux environnement var to build absolute path for logstash: `export PWD=$(pwd)`
2. Git change to the AGILE reproducibility branch:
    `git checkout agile-reproducibility`
3. Hydrate tweets (4hours are needed): `cd experiments/agile21/echen_input_filtred/tweets/ && python3 hydrate.py`
3. Prepare to unzip tweets: `mkdir hydrating-and-extracting && find . -name '*.jsonl.gz' -exec cp -prv '{}' 'hydrating-and-extracting' ';'`
3. Unzip all json.gz : `gunzip hydrating-and-extracting/id.jsonl.gz`
3. Change directory to be at the root of this repository: `cd $PWD`
3. Index and geocode Tweets: `sudo /usr/share/logstash/bin/logstash -f elasticsearch/logstash-config/json.conf`
3. As logstash is designed to run all the time (it's waiting for new files to index), we have to stop it after ~45min (to be sure that the 10K tweets have been indexed in elasticsearch. Note that you may not have 100% of successfully indexed tweets for several raisons: Twitter user may have deleted or changed the visibility of their tweets or/and photon/OSM didn't succeed in geocode their location): `CTRL^C`
3. Run the main script. It will process only the two last weeks of January 2020 (from 2020/01/22 to 2020/01/31)
    `python3 COVID-19-TweetIDS-ES-Analyse.py`
4. **Optional scripts**:
    + H-TFIDF wordcloud per week. `python3 exploration_data_analyse/COVID-19-TweetIDs-plot-agile.py`

### Some figures generated by the script:

| Description | script | Figure |
| ------ | ------ | ------ |
| Figure 3.Wordcloud of H-TFIDF terms for United Kingdom by week | exploration_data_analyse/COVID-19-TweetIDs-plot-agile.py | <img src="https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis/-/raw/agile-reproducibility/experiments/agile21/results/jan_2weeks_week/country/wordcloud/United_Kingdom_2020-01-26.png" width="500"/> |
| Figure 4. Common  terms  between  H-TFIDF,  TF-IDF  and most frequent terms for United Kingdom in the last week of February | COVID-19-TweetIDS-ES-Analyse.py | <img src="https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis/-/raw/agile-reproducibility/experiments/agile21/results/jan_2weeks_week/common/most_frequent_terms_by_country_United%20Kingdomvenn3.png" width="500"/> |
| Figure 5. Comparison of percentage of common words between H-TFIDF or TF-IDF with the most frequent terms percountry | COVID-19-TweetIDS-ES-Analyse.py | <img src="https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis/-/raw/agile-reproducibility/experiments/agile21/results/jan_2weeks_week/common/most_frequent_terms_by_country_100.png" width="500"/> |
| Figure 6a. Projection of H-TFIDF representation in a t-SNE space | COVID-19-TweetIDS-ES-Analyse.py | <img src="https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis/-/raw/agile-reproducibility/experiments/agile21/results/jan_2weeks_week/tsne/tsne_bert-embeddings_H-TFIDF.png" width="500"/> |
| Figure 6b. Projection of TF-IDF representation in a t-SNE space | COVID-19-TweetIDS-ES-Analyse.py | <img src="https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis/-/raw/agile-reproducibility/experiments/agile21/results/jan_2weeks_week/tsne/tsne_bert-embeddings_TF-IDF%20on%20corpus%20by%20Country.png" width="500"/> |


## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.

## Data Usage Agreement
By using the [E.Chen's dataset](https://github.com/echen102/COVID-19-TweetIDs), as [stated by the author](https://github.com/echen102/COVID-19-TweetIDs#data-usage-agreement) you agree to abide by the stipulations in the license, remain in compliance with Twitter’s [Terms of Service](https://developer.twitter.com/en/developer-terms/agreement-and-policy), and cite the following manuscript: 

Chen E, Lerman K, Ferrara E
Tracking Social Media Discourse About the COVID-19 Pandemic: Development of a Public Coronavirus Twitter Data Set
JMIR Public Health Surveillance 2020;6(2):e19273 
DOI: 10.2196/19273 
PMID: 32427106

## Acknowledgement
This repository uses a lot of awesome open source tools such as:
+ [sci-kit learn](https://scikit-learn.org/)
+ Python datascience librairies: Pandas, Numpy, Matplotlib
+ [Twarc](https://twarc-project.readthedocs.io/en/latest/) 
+ [Elastic stack](https://www.elastic.co/): logstash, elasticsearch, kibana
+ [Photon](https://photon.komoot.io/): geocoder using [OpenStreetMap](https://www.openstreetmap.org) Data 
